GCC = gcc

DRM_CFLAGS = -I/usr/include/libdrm 
DRM_LIBS = -ldrm 

# GTK3
GTK3_CFLAGS = -I/usr/include/gtk-3.0 -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/fribidi -I/usr/include/harfbuzz -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/uuid -I/usr/include/gio-unix-2.0 -I/usr/include/libdrm -I/usr/include/atk-1.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/at-spi-2.0 -I/usr/include/dbus-1.0 -I/usr/lib64/dbus-1.0/include -pthread  -DGDK_VERSION_MIN_REQUIRED=GDK_VERSION_3_22                                -DGDK_VERSION_MAX_ALLOWED=GDK_VERSION_3_22
GTK3_LIBS = -lgtk-3 -lgdk-3 -lpangocairo-1.0 -lpango-1.0 -latk-1.0 -lcairo-gobject -lcairo -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0 -lepoxy 

# GTK4
GTK4_CFLAGS = -I/usr/include/gtk-4.0 -I/usr/include/pango-1.0 -I/usr/include/glib-2.0 -I/usr/lib64/glib-2.0/include -I/usr/include/harfbuzz -I/usr/include/fribidi -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/graphene-1.0 -I/usr/lib64/graphene-1.0/include -mfpmath=sse -msse -msse2 -I/usr/include/atk-1.0 -I/usr/include/gio-unix-2.0 -pthread -I/usr/include/libdrm -I/usr/include/at-spi2-atk/2.0 -I/usr/include/at-spi-2.0 -I/usr/include/dbus-1.0 -I/usr/lib64/dbus-1.0/include 
GTK4_LIBS = -lgtk-4 -lz -lpangocairo-1.0 -lpango-1.0 -lharfbuzz -lgdk_pixbuf-2.0 -lcairo-gobject -lcairo -lgraphene-1.0 -latk-1.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0 

X_CFLAGS = 
X_LIBS = -lXfixes -lXrandr -lXinerama -lX11 

CFLAGS = -g -O2 -Wall -Werror $(DRM_CFLAGS) $(X_CFLAGS)
LDFLAGS = $(DRM_LIBS) $(X_LIBS)

all: waylandtest3 waylandtest4
.PHONY: all

%3.o: %.c
	$(GCC) -DGTK3 $(CFLAGS) $(GTK3_CFLAGS) -c $< -o $@
	
%4.o: %.c
	$(GCC) -DGTK4 $(CFLAGS) $(GTK4_CFLAGS) -c $< -o $@
	
clean:
	rm waylandtest3 waylandtest4
	rm *.o

waylandtest3: main3.o
	$(GCC) -o $@ $(LDFLAGS) $(GTK3_LIBS)  $<
	
waylandtest4: main4.o
	$(GCC) -o $@ $(LDFLAGS) $(GTK4_LIBS) $<
	